extern crate puzzle_3x3x3;
extern crate colored;
#[macro_use]
extern crate lazy_static;

use puzzle_3x3x3::*;
use puzzle_3x3x3::dim3::*;
use puzzle_3x3x3::piece::*;
use std::collections::{HashMap, HashSet};
use colored::Colorize;

fn main() {
	let space: HashSet<Point> = vec![
		Point(0, 0, 0),
		Point(0, 0, 1),
		Point(0, 0, 2),
		Point(0, 1, 0),
		Point(0, 1, 1),
		Point(0, 1, 2),
		Point(0, 2, 0),
		Point(0, 2, 1),
		Point(0, 2, 2),
		Point(1, 0, 0),
		Point(1, 0, 1),
		Point(1, 0, 2),
		Point(1, 1, 0),
		Point(1, 1, 1),
		Point(1, 1, 2),
		Point(1, 2, 0),
		Point(1, 2, 1),
		Point(1, 2, 2),
		Point(2, 0, 0),
		Point(2, 0, 1),
		Point(2, 0, 2),
		Point(2, 1, 0),
		Point(2, 1, 1),
		Point(2, 1, 2),
		Point(2, 2, 0),
		Point(2, 2, 1),
		Point(2, 2, 2),
	].iter().cloned().collect();
	let pieces: Vec<Piece> = vec![
		Piece([Point(0, 0, 0), Point(0, 1, 0), Point(0, 1, 1), Point(1, 1, 1)].iter().cloned().collect()), // White
		Piece([Point(1, 0, 0), Point(1, 1, 0), Point(1, 1, 1), Point(0, 1, 1)].iter().cloned().collect()), // Brown
		Piece([Point(0, 0, 0), Point(1, 0, 0), Point(0, 1, 0), Point(0, 0, 1)].iter().cloned().collect()), // Red
		Piece([Point(0, 0, 0), Point(1, 0, 0), Point(1, 1, 0), Point(2, 0, 0)].iter().cloned().collect()), // Yellow
		Piece([Point(0, 0, 0), Point(1, 0, 0), Point(2, 0, 0), Point(2, 1, 0)].iter().cloned().collect()), // Blue
		Piece([Point(0, 0, 0), Point(1, 0, 0), Point(1, 1, 0), Point(2, 1, 0)].iter().cloned().collect()), // Black
		Piece([Point(0, 0, 0), Point(1, 0, 0), Point(0, 1, 0)].iter().cloned().collect()),                 // Green
	];
	let mut optional_filters: HashMap<usize, (bool, Option<Box<Fn(&Piece)->Vec<Piece>>>)> = HashMap::new();
	optional_filters.insert(0 as usize, (
		true,
		Some(Box::new(|piece: &Piece| vec![Matrix((0, -1, 0), (-1, 0, 0), (0, 0, -1)) * piece.clone() + Point(2, 2, 2)]))
	));
	let answers = solve(&space, &pieces, &optional_filters);
	println!("{} answers found", answers.len());
	println!("Answers:");
	let range = range(&space.clone());
	for answer in answers {
		print(&answer, &range);
	}
}

fn range(answer: &HashSet<Point>) -> ([isize; 2], [isize; 2], [isize; 2]) {
	let mut xr = [std::isize::MAX, std::isize::MIN];
	let mut yr = [std::isize::MAX, std::isize::MIN];
	let mut zr = [std::isize::MAX, std::isize::MIN];
	for p in answer.iter() {
		if p.0 < xr[0] {
			xr[0] = p.0;
		}
		if p.0 > xr[1] {
			xr[1] = p.0;
		}
		if p.1 < yr[0] {
			yr[0] = p.1;
		}
		if p.1 > yr[1] {
			yr[1] = p.1;
		}
		if p.2 < zr[0] {
			zr[0] = p.2;
		}
		if p.2 > zr[1] {
			zr[1] = p.2;
		}
	}

	(xr, yr, zr)
}

lazy_static! {
	static ref COLORS: Vec<&'static str> = vec!["white", "magenta", "red", "yellow", "cyan", "blue", "green"];
}

fn print(answer: &HashMap<Point, usize>, range: &([isize; 2], [isize; 2], [isize; 2])) {
	for y in range.1[0] .. range.1[1] + 1 {
		for z in range.2[0] .. range.2[1] + 1 {
			if z != range.2[0] {
				print!(" ");
			}
			for x in range.0[0] .. range.0[1] + 1 {
				if let Some(color_no) = answer.get(&Point(x, y, z)) {
					print!("{}", format!("{}", color_no).color(COLORS[*color_no]).reversed());
				} else {
					print!(" ");
				}
			}
		}
		println!();
	}
	println!();
}
