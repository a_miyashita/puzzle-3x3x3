#[macro_use]
extern crate lazy_static;

pub mod dim3 {
	use std::ops::*;
	use std::collections::HashSet;

	#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
	pub struct Point(pub isize, pub isize, pub isize);

	impl Add for Point {
		type Output = Point;

		fn add(self, rhs: Point) -> Point {
			Point(self.0 + rhs.0, self.1 + rhs.1, self.2 + rhs.2)
		}
	}

	impl Neg for Point {
		type Output = Point;

		fn neg(self) -> Point {
			Point(-self.0, -self.1, -self.2)
		}
	}

	impl Sub for Point {
		type Output = Point;

		fn sub(self, rhs: Point) -> Point {
			Point (self.0 - rhs.0, self.1 - rhs.1, self.2 - rhs.2)
		}
	}

	#[cfg(test)]
	mod add_test {
		use super::Point;

		#[test]
		fn add() {
			let a = Point(1, 2, 3);
			let b = Point(4, 5, 6);
			let expected = Point (5, 7, 9);
			let actual = a + b;
			assert_eq!(expected, actual);
		}
	}

	#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
	pub struct Matrix(pub (isize, isize, isize), pub (isize, isize, isize), pub (isize, isize, isize));

	impl Mul for Matrix {
		type Output = Matrix;

		fn mul(self, rhs: Matrix) -> Matrix {
			Matrix(
				(
					(self.0).0 * (rhs.0).0 + (self.0).1 * (rhs.1).0 + (self.0).2 * (rhs.2).0,
					(self.0).0 * (rhs.0).1 + (self.0).1 * (rhs.1).1 + (self.0).2 * (rhs.2).1,
					(self.0).0 * (rhs.0).2 + (self.0).1 * (rhs.1).2 + (self.0).2 * (rhs.2).2,
				), (
					(self.1).0 * (rhs.0).0 + (self.1).1 * (rhs.1).0 + (self.1).2 * (rhs.2).0,
					(self.1).0 * (rhs.0).1 + (self.1).1 * (rhs.1).1 + (self.1).2 * (rhs.2).1,
					(self.1).0 * (rhs.0).2 + (self.1).1 * (rhs.1).2 + (self.1).2 * (rhs.2).2,
				), (
					(self.2).0 * (rhs.0).0 + (self.2).1 * (rhs.1).0 + (self.2).2 * (rhs.2).0,
					(self.2).0 * (rhs.0).1 + (self.2).1 * (rhs.1).1 + (self.2).2 * (rhs.2).1,
					(self.2).0 * (rhs.0).2 + (self.2).1 * (rhs.1).2 + (self.2).2 * (rhs.2).2,
				)
			)
		}
	}

	impl Mul<Point> for Matrix {
		type Output = Point;

		fn mul(self, rhs: Point) -> Point {
			Point(
				(self.0).0 * rhs.0 + (self.0).1 * rhs.1 + (self.0).2 * rhs.2,
				(self.1).0 * rhs.0 + (self.1).1 * rhs.1 + (self.1).2 * rhs.2,
				(self.2).0 * rhs.0 + (self.2).1 * rhs.1 + (self.2).2 * rhs.2,
			)
		}
	}

	pub const E: Matrix = Matrix((1, 0, 0), (0, 1, 0), (0, 0, 1));
	pub const RX: Matrix = Matrix((1, 0, 0,), (0, 0, -1), (0, 1, 0));
	pub const RY: Matrix = Matrix((0, 0, 1), (0, 1, 0), (-1, 0, 0));
	pub const RZ: Matrix = Matrix((0, -1, 0), (1, 0, 0), (0, 0, 1));

	lazy_static! {
		pub static ref ROT_MATRIX: HashSet<Matrix> = vec![E].into_iter()
			.flat_map(|m| vec![m, m*RX, m*RX*RX, m*RX*RX*RX])
			.flat_map(|m| vec![m, m*RY, m*RY*RY, m*RY*RY*RY])
			.flat_map(|m| vec![m, m*RZ, m*RZ*RZ, m*RZ*RZ*RZ])
			.collect();
	}

	#[cfg(test)]
	mod matrix_test {
		use super::{Matrix, E};

		#[test]
		fn mul() {
			let a = Matrix((2, 8, 4), (7, 5, 3), (6, 1, 9));
			let b = Matrix((1, 2, 3), (4, 5, 6), (7, 8, 9));
			let expected = Matrix((62, 76, 90), (48, 63, 78), (73, 89, 105));
			let actual = a * b;
			assert_eq!(actual, expected);
		}

		#[test]
		fn mul_e_1() {
			let a = Matrix((2, 8, 4), (7, 5, 3), (6, 1, 9));
			let actual = E * a;
			assert_eq!(actual, a);
		}

		#[test]
		fn mul_e_2() {
			let a = Matrix((2, 8, 4), (7, 5, 3), (6, 1, 9));
			let actual = a * E;
			assert_eq!(actual, a);
		}
	}
}

pub mod piece {
	use super::dim3::{Point, Matrix, ROT_MATRIX, E};
	use std;
	use std::hash::{Hash, Hasher};
	use std::collections::HashSet;
	use std::ops::{Add, Mul};
	use std::cmp::Ordering;

	#[derive(Debug, Clone, Eq, PartialEq)]
	pub struct Piece(pub HashSet<Point>);

	impl Hash for Piece {
		fn hash<H: Hasher>(&self, state: &mut H) {
			let mut points: Vec<Point> = self.0.clone().into_iter().collect();
			points.sort_unstable_by(|x, y| {
				match x.0.cmp(&y.0) {
					Ordering::Equal => match x.1.cmp(&y.1) {
						Ordering::Equal => x.2.cmp(&y.2),
						c => c,
					},
					c => c,
				}
			});
			points.hash(state);
		}
	}

	impl Mul<Piece> for Matrix {
		type Output=Piece;
		fn mul(self, rhs: Piece) -> Piece {
			Piece(
				rhs.0.into_iter()
					.map(|point| self * point)
					.collect()
			)
		}
	}

	impl Add<Point> for Piece {
		type Output=Piece;
		fn add(self, rhs: Point) -> Piece {
			Piece(
				self.0.into_iter()
					.map(|p| p + rhs)
					.collect()
			)
		}
	}

	pub fn available_assignments(
		piece: &Piece,
		space: &HashSet<Point>,
		suppress_rotation: bool,
		removal_filter: &Option<Box<Fn(&Piece) -> Vec<Piece>>>,
	) -> HashSet<Piece> {
		let mut xr = [std::isize::MAX, std::isize::MIN];
		let mut yr = [std::isize::MAX, std::isize::MIN];
		let mut zr = [std::isize::MAX, std::isize::MIN];
		for p in space {
			if p.0 < xr[0] {
				xr[0] = p.0;
			}
			if p.0 > xr[1] {
				xr[1] = p.0;
			}
			if p.1 < yr[0] {
				yr[0] = p.1;
			}
			if p.1 > yr[1] {
				yr[1] = p.1;
			}
			if p.2 < zr[0] {
				zr[0] = p.2;
			}
			if p.2 > zr[1] {
				zr[1] = p.2;
			}
		}

		let rots: HashSet<Matrix> = if suppress_rotation {
			vec![E].into_iter().collect()
		} else {
			ROT_MATRIX.clone()
		};
		let mut result: HashSet<Piece> = rots.iter()
			.map(|rot| *rot * piece.clone())
			.flat_map(|p| range_x(&p, xr))
			.flat_map(|p| range_y(&p, yr))
			.flat_map(|p| range_z(&p, zr))
			.filter(|p| {
				p.0.iter().all(|point| space.contains(point))
			})
			.collect();
		if let Some(removal_filter) = removal_filter {
			let mut to_be_removed: HashSet<Piece> = HashSet::new();
			for piece in result.iter() {
				if to_be_removed.contains(piece) {
					continue;
				}
				for p in removal_filter(piece).into_iter() {
					to_be_removed.insert(p);
				}
			}
			for piece in to_be_removed {
				result.remove(&piece);
			}
		}
		//println!("len {:?}", &result.len());
		result
	}

	fn range_point(
		piece: &Piece,
		range_inclusive: [isize; 2],
		extract_value: impl Fn(&Point) -> isize,
		gen_value: impl Fn(&Point, isize) -> Point,
	) -> HashSet<Piece> {
		let mut cmin = std::isize::MAX;
		let mut cmax = std::isize::MIN;
		for point in piece.0.iter() {
			let v = extract_value(&point);
			if v < cmin {
				cmin = v;
			}
			if cmax < v {
				cmax = v;
			}
		}
		let mut result = HashSet::new();
		let start = cmin - range_inclusive[0];
		let end = range_inclusive[1] - cmax;
		for i in start..end + 1 {
			let mut p = HashSet::new();
			for point in &piece.0 {
				p.insert(gen_value(&point, i));
			}
			result.insert(Piece(p));
		}
		result
	}

	fn range_x(piece: &Piece, range_inclusive: [isize; 2]) -> HashSet<Piece> {
		range_point(
			piece,
			range_inclusive,
			|p| p.0,
			|p, i| Point(p.0 + i, p.1, p.2)
		)
	}

	fn range_y(piece: &Piece, range_inclusive: [isize; 2]) -> HashSet<Piece> {
		range_point(
			piece,
			range_inclusive,
			|p| p.1,
			|p, i| Point(p.0, p.1 + i, p.2)
		)
	}

	fn range_z(piece: &Piece, range_inclusive: [isize; 2]) -> HashSet<Piece> {
		range_point(
			piece,
			range_inclusive,
			|p| p.2,
			|p, i| Point(p.0, p.1, p.2 + i)
		)
	}

	#[cfg(test)]
	mod test {
		use super::*;

		#[test]
		fn test_range_x() {
			let piece = Piece(vec![
				Point(0, 0, 0),
				Point(1, 0, 0),
				Point(0, 1, 0),
			].iter().cloned().collect());
			let actual = range_x(&piece, [0, 2]);
			let expected: HashSet<Piece> = vec![
				Piece(vec![
					Point(0, 0, 0),
					Point(1, 0, 0),
					Point(0, 1, 0),
				].iter().cloned().collect()),
				Piece(vec![
					Point(1, 0, 0),
					Point(2, 0, 0),
					Point(1, 1, 0),
				].iter().cloned().collect()),
			].iter().cloned().collect();
			assert_eq!(actual, expected);
		}

		#[test]
		fn test_range_y() {
			let piece = Piece(vec![
				Point(0, 0, 0),
				Point(1, 0, 0),
				Point(0, 1, 0),
			].iter().cloned().collect());
			let actual = range_y(&piece, [0, 2]);
			let expected: HashSet<Piece> = vec![
				Piece(vec![
					Point(0, 0, 0),
					Point(1, 0, 0),
					Point(0, 1, 0),
				].iter().cloned().collect()),
				Piece(vec![
					Point(0, 1, 0),
					Point(1, 1, 0),
					Point(0, 2, 0),
				].iter().cloned().collect()),
			].iter().cloned().collect();
			assert_eq!(actual, expected);
		}

		#[test]
		fn test_range_z() {
			let piece = Piece(vec![
				Point(0, 0, 0),
				Point(1, 0, 0),
				Point(0, 1, 0),
			].iter().cloned().collect());
			let actual = range_z(&piece, [0, 2]);
			let expected: HashSet<Piece> = vec![
				Piece(vec![
					Point(0, 0, 0),
					Point(1, 0, 0),
					Point(0, 1, 0),
				].iter().cloned().collect()),
				Piece(vec![
					Point(0, 0, 1),
					Point(1, 0, 1),
					Point(0, 1, 1),
				].iter().cloned().collect()),
				Piece(vec![
					Point(0, 0, 2),
					Point(1, 0, 2),
					Point(0, 1, 2),
				].iter().cloned().collect()),
			].iter().cloned().collect();
			assert_eq!(actual, expected);
		}
	}
}

use std::collections::{HashSet, HashMap};
use dim3::*;
use piece::{Piece, available_assignments};

pub fn solve(
	space: &HashSet<Point>, pieces: &Vec<Piece>,
	optiona_filters: &HashMap<usize, (bool, Option<Box<Fn(&Piece)->Vec<Piece>>>)>
) -> Vec<HashMap<Point, usize>> {
	let available_assignments: Vec<HashSet<Piece>> = pieces.iter()
			.zip(0..)
			.map(|(piece, n)| {
				let filter = optiona_filters.get(&n).unwrap_or(&(false, None));
				available_assignments(piece, &space, filter.0, &filter.1)
			})
			.collect();

	let mut count = 0;
	let mut found = 0;

	do_solve(&mut count, &mut found,0, HashMap::new(), &available_assignments)
}

fn do_solve(count: &mut usize, found: &mut usize, i: usize, current: HashMap<Point, usize>, available_assignments: &Vec<HashSet<Piece>>) -> Vec<HashMap<Point, usize>> {
	*count += 1;
//	if *count % 1000 == 0 {
//		println!("{} checked", count);
//	}
	let pieces = available_assignments.get(i).unwrap();
	let mut answer = vec![];
	let last_piece = i == available_assignments.len() - 1;
	for piece in pieces {
		let collided = piece.0.iter().any(|point| current.contains_key(point));
		if collided {
			continue;
		}
		let mut current_copied = current.clone();
		for point in &piece.0 {
			current_copied.insert(point.clone(), i);
		}
		if last_piece {
			*found += 1;
//			println!("Found ({}), {:?}", found, &current_copied);
			answer.push(current_copied);
		} else {
			answer.append(&mut do_solve(count, found, i + 1, current_copied, available_assignments));
		}
	}
	answer
}
